#include "glwidget.h"
#include "qdebug.h"
#include <QMouseEvent>



GLWidget::GLWidget()
{
}


void GLWidget::vboSetup()
{
    uint Index[] = {0,1,4, 1,3,5, 3,2,6,0,7,2, 0,2,3, 0,1,3};
    // definicja wierzcholkow
    vec3f verts_buf[] = { {-1,-1,0}, {1,-1,0}, {-1,-1,1},{1,-1,1}, {0,1,0.5}, {0,1,0.5}, {0,1,0.5},{0,1,0.5} };
    vec3f colors[] = { {1,0,0}, {0,1,0}, {0,0,1},{1,1,0}, {0,1,0.5}, {0,1,0.5}, {0,1,0.5}, {0,1,0.5} };

    memcpy( uIndex, Index, 18*4 );
    memcpy( uColors, colors, 15*4 );
    memcpy( uVerts, verts_buf, 15*4 );


    // wygenerowanie nazwy/numeru uchwytu dla obiektu buforowego vbo
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &cbo);
    glGenBuffers(1, &IndexBufferId);

    // ustawienie bufora 'vbo' jako aktualnego bufora tablicowego (ARRAY_BUFFER) (zwiazanie/aktywowanie bufora)
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, cbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferId);

    // rezerwacja pamieci dla aktualnego bufora (po stronie GPU) i wypelnienie go danymi z tablicy verts_buf
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts_buf), verts_buf, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index), Index, GL_STATIC_DRAW);

    // odwiazanie aktualnego bufora
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void GLWidget::initializeGL()
{
    // obowiązkowe wywołanie metody z klasy QtOpenGLFunctions, które inicjuje wskaźniki do funkcji OGLa
    initializeOpenGLFunctions();  

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0.0f, 0.0f, 0.1f, 0.0f);


    vboSetup();

    PRINT_GL_ERRORS("initializeGL(): ");
}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0,0, w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, w/(GLfloat)h, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glPushMatrix();

    glTranslatef(0.0f, 0.0f, -3.5f);

    glRotatef(25,0,1,0);
    // buffer mode

    glColor3f(0.5, 0.7, 0.5);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);   // zwiazanie bufora vbo z aktualnym
        glBindBuffer(GL_ARRAY_BUFFER, cbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferId);

        glColorPointer(3, GL_FLOAT, 0, (const GLvoid*)0);
        glVertexPointer(3, GL_FLOAT, 0, (const GLvoid*)0);

        glDrawElements(GL_TRIANGLES, sizeof(uVerts), GL_UNSIGNED_INT, (const GLvoid*)0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    glPopMatrix();
    PRINT_GL_ERRORS("paintGL(): ");
}
