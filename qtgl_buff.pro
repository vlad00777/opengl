QT += opengl widgets

SOURCES += \
    main.cpp \
    glwidget.cpp

HEADERS += \
    glwidget.h

win32{
    LIBS += -lglu32
    LIBS += -lopengl32
}

CONFIG += debug console

unix{
    LIBS = -lGLU
}
