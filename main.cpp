#include <QApplication>
#include "glwidget.h"


int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
    QApplication app(argc, argv);

    GLWidget w;
    w.show();

    return app.exec();
}
