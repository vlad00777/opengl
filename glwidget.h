#ifndef GLWIDGET_H
#define GLWIDGET_H

// w przypadku linuxa ta linia aktywuje wszystkie funkcje opengl
#define GL_GLEXT_PROTOTYPES 1

#include <QDebug>
#include <QOpenGLFunctions>
#include <QGLWidget>
#include <GL/glu.h>

// prosta struktura wektora 3d
struct vec3f {GLfloat x,y,z;};
struct vec3b {GLubyte x,y,z;};

#define PRINT_GL_ERRORS(mess) {GLenum err; while( (err=glGetError()) != GL_NO_ERROR) { qDebug() << mess << (const char*)gluErrorString(err); } }

// Dziedziczenie po QOpenGLFunctions zapewnia dostęp do wszystkich funkcji OGL
class GLWidget : public QGLWidget, protected QOpenGLFunctions
{
    GLuint vbo;   // uchwyt do bufora wierzcholkow - Vertex Buffer Object
    GLuint IndexBufferId;
    GLuint cbo;
    uint uIndex[18];
    vec3f uColors[5];
    vec3f uVerts[5];

    void vboSetup();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

public:
    GLWidget();
    double angle_x;
    double angle_z;
    double mouse_x;
    double mouse_y;
    //void mouseMoveEvent(QMouseEvent *me);
    //void rotate();
};


#endif // GLWIDGET_H
